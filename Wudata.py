from bs4 import BeautifulSoup
import requests
import datetime
import json
from Response import Response

def get_icaocode(location:str):
    wu_url = 'https://www.wunderground.com/calendar/'+location
    source = requests.get(wu_url).text
    soup = BeautifulSoup(source, 'lxml')
    icaocode = str(soup.find('li', class_='selected').a['href']).split('/')[4]
    return icaocode

def get_historical_data(location:str,start_date:datetime,end_date:datetime):
    icao_code = get_icaocode(location)
    date_range = str(start_date.year)+str('{:02d}'.format(start_date.month))+str('{:02d}'.format(start_date.day))+\
                 str(end_date.year)+str('{:02d}'.format(end_date.month))+str('{:02d}'.format(end_date.day))
    wu_base_url = 'https://api-ak.wunderground.com/api/d8585d80376a429e/history_'+date_range+ '/lang:EN/units:english/bestfct:1/v:2.0/q/'+icao_code+'.json?showObs=0&ttl=120'
    source = requests.get(wu_base_url).text
    soup = BeautifulSoup(source, 'lxml')
    histories = json.loads(soup.find('p').text)['history']['days']
    responses = []
    for history in histories:
        summary = history['summary']
        response = Response(summary['avgoktas'],summary['coolingdegreedays'],str(datetime.date(summary['date']['year'], summary['date']['month'], summary['date']['day'])),
                            summary['dewpoint'],summary['gdegreedays'],
                            summary['max_dewpoint'],summary['max_humidity'],summary['max_pressure'],summary['max_temperature'],
                            summary['max_temperature_record'], summary['max_temperature_record_year'],summary['max_visibility'],
                            summary['max_wind_speed'],summary['min_dewpoint'],summary['min_humidity'],summary['min_pressure'],
                            summary['min_temperature'],summary['min_temperature_record'],summary['min_temperature_record_year'],
                            summary['min_visibility'], summary['min_wind_speed'], summary['preciprecordyear'],summary['pressure'],
                            summary['temperature'],summary['visibility'],summary['wind_speed'])
        responses.append(response)
    json_string = json.dumps([ob.__dict__ for ob in responses])

    return json_string


